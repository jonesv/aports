# Contributor: Leon Marz <main@lmarz.org>
# Maintainer: Leon Marz <main@lmarz.org>
pkgname=level-zero
pkgver=1.15.13
pkgrel=0
pkgdesc="oneAPI Level Zero Loader"
url="https://spec.oneapi.com/versions/latest/elements/l0/source/index.html"
arch="all"
license="MIT"
makedepends="cmake samurai"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/oneapi-src/level-zero/archive/v$pkgver.tar.gz"
options="!check" # no testsuite

build() {
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=ON \
		-DCMAKE_BUILD_TYPE=None

	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev
	amove usr/lib/libze_tracing_layer.so*
	amove usr/lib/libze_validation_layer.so*
}

sha512sums="
93fe21c5722443b7cf7293c4e8d8e3b5fc07749933ea1145c8884f919b06617ecd20b382bed9eeef585024e4ec5b27c0548ac05aef2bb92bd5356e1bac8e29d2  level-zero-1.15.13.tar.gz
"
